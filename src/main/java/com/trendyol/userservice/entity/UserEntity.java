package com.trendyol.userservice.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "users")
@Getter
@Setter
public class UserEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer id;
    public String userName;
    public String password;
    public String name;
    public String surName;
    public String email;
    public String phoneNumber;
    public String address;
    public Date createDate;
    public Date updateDate;
}
