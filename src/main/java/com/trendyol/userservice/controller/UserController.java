package com.trendyol.userservice.controller;

import com.trendyol.userservice.entity.UserEntity;
import com.trendyol.userservice.service.UserService;
import javassist.NotFoundException;
import lombok.RequiredArgsConstructor;
import org.apache.catalina.User;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/user")
@RequiredArgsConstructor
public class UserController {
    public final UserService userService;

    @PostMapping
    public ResponseEntity<?> saveUser(@RequestBody UserEntity userEntity) throws Exception {
        return ResponseEntity.status(HttpStatus.OK).body(userService.saveUser(userEntity));
    }

    @GetMapping
    public ResponseEntity<?> getUser(@RequestParam(value = "userId") Integer userId){
        return ResponseEntity.status(HttpStatus.OK).body(userService.getUser(userId));
    }

    @DeleteMapping
    public ResponseEntity<?> deleteUser(@RequestParam(value = "userId") Integer userId) {
        userService.deleteUser(userId);
        return ResponseEntity.status(HttpStatus.OK).body("User is removed.");
    }

    @PutMapping
    public void updateUser(@RequestBody UserEntity userEntity){
        userService.updateUser(userEntity);
    }

    @GetMapping("/list")
    public List<UserEntity> getUsersForProduct(@RequestParam(value = "userIds") List<Integer> userIds){
        return userService.getUsers(userIds);
    }
}
