package com.trendyol.userservice.service;

import com.trendyol.userservice.entity.UserEntity;
import com.trendyol.userservice.repository.UserRepository;
import javassist.NotFoundException;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class UserService {
    public final UserRepository userRepository;

    public UserEntity saveUser(UserEntity userEntity) throws Exception {
        if(StringUtils.isEmpty(userEntity.getUserName())){
            throw new Exception("User name can't be empty");
        }
        if(StringUtils.isEmpty(userEntity.getPassword())){
            throw new Exception("Password can't be empty");
        }
        if(StringUtils.isEmpty(userEntity.getEmail())){
            throw new Exception("Email can't be empty");
        }
        if(StringUtils.isEmpty(userEntity.getPhoneNumber())){
            throw new Exception("Phone number can't be empty");
        }
        return userRepository.save(userEntity);
    }

    public UserEntity getUser(Integer userId){ return userRepository.findById(userId).orElse(null); }

    public void deleteUser(Integer userId){
        userRepository.deleteById(userId);
    }

    public void updateUser(UserEntity userEntity){
        Optional<UserEntity> entity = userRepository.findById(userEntity.getId());
        entity.ifPresent(user -> {
            userRepository.save(userEntity);
        });
    }

    public List<UserEntity> getUsers(List<Integer> userIds){
        return userRepository.findByIdIn(userIds);
    }
}
