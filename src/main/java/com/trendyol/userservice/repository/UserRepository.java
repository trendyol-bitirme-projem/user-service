package com.trendyol.userservice.repository;

import com.trendyol.userservice.entity.UserEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends CrudRepository<UserEntity, Integer> {
    List<UserEntity> findByIdIn(List<Integer> userIds);
}
